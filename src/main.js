// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import './stylus/main.styl'
import App from './App'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSweetAlert from 'vue-sweetalert'
// import underscore from 'vue-underscore'
import lodash from 'lodash'
import VueLodash from 'vue-lodash'
// import VueVirtualScroller from 'vue-virtual-scroller'
import AsyncComputed from 'vue-async-computed'
import 'jquery-resizable-dom'
import fullscreen from 'vue-fullscreen'

Vue.use(Vuetify)
Vue.use(VueAxios, axios)
Vue.use(VueSweetAlert)
// Vue.use(underscore)
Vue.use(VueLodash, lodash)
// Vue.use(VueVirtualScroller)
Vue.use(AsyncComputed)
Vue.use(fullscreen)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
