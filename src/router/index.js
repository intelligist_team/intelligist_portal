import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/auth/login'
import navigationbar from '@/components/navigationbar'
import help from '@/components/help'
import dataPreparation from '@/components/dataPreparation'
import metaData from '@/components/sideMenu/metaData'
import intelligistSharing from '@/components/sideMenu/intelligistSharing'
import managementConsole from '@/components/sideMenu/managementConsole'
import socialMonitoring from '@/components/sideMenu/social'
import generatewizard from '@/components/sideMenu/generatewizard'
import mgmtUsers from '@/components/management/mgmtUsers'
import mgmtGroups from '@/components/management/mgmtGroups'
import mgmtRoles from '@/components/management/mgmtRoles'
import mgmtDashboard from '@/components/management/mgmtDashboard'
import mgmtProjects from '@/components/management/mgmtProjects'
import mgmtSubscription from '@/components/management/mgmtSubscription'
import dashboard from '@/components/sideMenu/dashboard'
import bank from '@/components/dashboard/bank'
import education from '@/components/dashboard/education'
import government from '@/components/dashboard/government'
import finance from '@/components/dashboard/finance'
import health from '@/components/dashboard/health'
import commercial from '@/components/dashboard/commercial'
import fishery from '@/components/dashboard/fishery'
import inet from '@/components/dashboard/inet'
import uploadMornitoring from '@/components/sideMenu/uploadMornitoring'
import signup from '@/components/auth/signup'
import test from '@/components/test'
import test2 from '@/components/test2'
import test3 from '@/components/test3'
import sorry from '@/components/sorry'
import portal from '@/components/portal'
import graphed1 from '@/components/dashboard/detail/graphED1'
import graphed2 from '@/components/dashboard/detail/graphED2'
import graphed3 from '@/components/dashboard/detail/graphED3'
import graphed4 from '@/components/dashboard/detail/graphED4'
import graphed5 from '@/components/dashboard/detail/graphED5'
import graphed6 from '@/components/dashboard/detail/graphED6'
import graphgrovindonesiacoal from '@/components/dashboard/detail/graphGrovIndonesiaCoal'
import graphgrovrain from '@/components/dashboard/detail/graphGrovRain'
import graphcombanpu from '@/components/dashboard/detail/graphComBanpu'
import graphcomsurapon from '@/components/dashboard/detail/graphComSurapon'
import graphcomhotpotnewsales from '@/components/dashboard/detail/graphComHotpotNewSales'
import graphcomhotpottopsale from '@/components/dashboard/detail/graphComHotpotTopSale'
import graphfin1 from '@/components/dashboard/detail/graphFin1'
import graphhealth1 from '@/components/dashboard/detail/graphHealth1'
import graphbank1 from '@/components/dashboard/detail/graphBank1'
import graphInsurance from '@/components/dashboard/detail/graphInsurance'
import graphfish1 from '@/components/dashboard/detail/graphFishery1'
import graphfish2 from '@/components/dashboard/detail/graphFishery2'
import graphfish3 from '@/components/dashboard/detail/graphFishery3'
import graphfish4 from '@/components/dashboard/detail/graphFishery4'
import graphfish5 from '@/components/dashboard/detail/graphFishery5'
import graphinet1 from '@/components/dashboard/detail/graphInet1'
import verify from '@/components/auth/verify'
import forgotpassword from '@/components/auth/forgotpassword'
import resetpassword from '@/components/auth/resetpassword'
import viewdataworkbench from '@/components/sideMenu/dialogviewer/viewdataworkbench'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    { path: '/navigationbar', component: navigationbar, meta: {requiresAuth: true} },
    { path: '/', component: login },
    { path: '/sorry', component: sorry },
    { path: '/portal',
      component: portal,
      children: [
        { path: '/help', component: help },
        { path: '/metadata', component: metaData, meta: {requiresAuth: true, UACMetadata: true} },
        { path: '/dashboard',
          component: dashboard,
          meta: {requiresAuth: true, UACDashboard: true},
          children: [
            { path: '/dashboard/bank', component: bank, meta: {requiresAuth: true, UACDashboard: true, UACbank: true} },
            { path: '/dashboard/education', component: education, meta: {requiresAuth: true, UACDashboard: true, UACeducation: true} },
            { path: '/dashboard/government', component: government, meta: {requiresAuth: true, UACDashboard: true, UACgovernment: true} },
            { path: '/dashboard/finance', component: finance, meta: {requiresAuth: true, UACDashboard: true, UACfinance: true} },
            { path: '/dashboard/health', component: health, meta: {requiresAuth: true, UACDashboard: true, UAChealth: true} },
            { path: '/dashboard/commercial', component: commercial, meta: {requiresAuth: true, UACDashboard: true, UACcommercial: true} },
            { path: '/dashboard/fishery', component: fishery, meta: {requiresAuth: true, UACDashboard: true, UACfishery: true} },
            { path: '/dashboard/inet', component: inet, meta: {requiresAuth: true, UACDashboard: true, UACfishery: true} }
          ]
        },
        { path: '/uploadmornitoring', component: uploadMornitoring, meta: {requiresAuth: true, UACUploadmornitoring: true} },
        { path: '/datapreparation', component: dataPreparation, meta: {requiresAuth: true, UACDataprep: true} },
        { path: '/intelligistsharing', component: intelligistSharing, meta: {requiresAuth: true, UACIntelligistsharing: true} },
        { path: '/managementconsole',
          component: managementConsole,
          meta: {requiresAuth: true, requiresAuthAdmin: true},
          children: [
            { path: '/management/mgmtusers', component: mgmtUsers, meta: {requiresAuth: true, requiresAuthAdmin: true} },
            { path: '/management/mgmtgroups', component: mgmtGroups, meta: {requiresAuth: true, requiresAuthAdmin: true} },
            { path: '/management/mgmtroles', component: mgmtRoles, meta: {requiresAuth: true, requiresAuthAdmin: true} },
            { path: '/management/mgmtdashboard', component: mgmtDashboard, meta: {requiresAuth: true, requiresAuthAdmin: true} },
            { path: '/management/mgmtprojects', component: mgmtProjects, meta: {requiresAuth: true, requiresAuthAdmin: true} },
            { path: '/management/mgmtsubscription', component: mgmtSubscription, meta: {requiresAuth: true, requiresAuthAdmin: true} }
          ]
        },
        { path: '/social', component: socialMonitoring, meta: {requiresAuth: true, UACSocialMonitoring: true} },
        { path: '/generatewizard', component: generatewizard, meta: {requiresAuth: true, UACGenerateWizard: true} }
      ]
    },
    { path: '/test', component: test },
    { path: '/test2', component: test2 },
    { path: '/test3', component: test3 },
    { path: '/signup', component: signup },
    { path: '/verify/:id', component: verify },
    { path: '/forgotpassword', component: forgotpassword },
    { path: '/resetpassword/:id', component: resetpassword },
    { path: '/dashboard/education/graphed1', component: graphed1, meta: {requiresAuth: true, UACDashboard: true, UACeducation: true} },
    { path: '/dashboard/education/graphed2', component: graphed2, meta: {requiresAuth: true, UACDashboard: true, UACeducation: true} },
    { path: '/dashboard/education/graphed3', component: graphed3, meta: {requiresAuth: true, UACDashboard: true, UACeducation: true} },
    { path: '/dashboard/education/graphed4', component: graphed4, meta: {requiresAuth: true, UACDashboard: true, UACeducation: true} },
    { path: '/dashboard/education/graphed5', component: graphed5, meta: {requiresAuth: true, UACDashboard: true, UACeducation: true} },
    { path: '/dashboard/education/graphed6', component: graphed6, meta: {requiresAuth: true, UACDashboard: true, UACeducation: true} },
    { path: '/dashboard/government/graphgrovindonesiacoal', component: graphgrovindonesiacoal, meta: {requiresAuth: true, UACDashboard: true, UACgovernment: true} },
    { path: '/dashboard/government/graphgrovrain', component: graphgrovrain, meta: {requiresAuth: true, UACDashboard: true, UACgovernment: true} },
    { path: '/dashboard/commercial/graphcombanpu', component: graphcombanpu, meta: {requiresAuth: true, UACDashboard: true, UACcommercial: true} },
    { path: '/dashboard/commercial/graphcomsurapon', component: graphcomsurapon, meta: {requiresAuth: true, UACDashboard: true, UACcommercial: true} },
    { path: '/dashboard/commercial/graphcomhotpotnewsales', component: graphcomhotpotnewsales, meta: {requiresAuth: true, UACDashboard: true, UACcommercial: true} },
    { path: '/dashboard/commercial/graphcomhotpottopsale', component: graphcomhotpottopsale, meta: {requiresAuth: true, UACDashboard: true, UACcommercial: true} },
    { path: '/dashboard/finance/graphfin1', component: graphfin1, meta: {requiresAuth: true, UACDashboard: true, UACfinance: true} },
    { path: '/dashboard/health/graphhealth1', component: graphhealth1, meta: {requiresAuth: true, UACDashboard: true, UAChealth: true} },
    { path: '/dashboard/bank/graphbank1', component: graphbank1, meta: {requiresAuth: true, UACDashboard: true, UACbank: true} },
    { path: '/dashboard/bank/graphInsurance', component: graphInsurance, meta: {requiresAuth: true, UACDashboard: true, UACbank: true} },
    { path: '/dashboard/fishery/graphfish1', component: graphfish1, meta: {requiresAuth: true, UACDashboard: true, UACfishery: true} },
    { path: '/dashboard/fishery/graphfish2', component: graphfish2, meta: {requiresAuth: true, UACDashboard: true, UACfishery: true} },
    { path: '/dashboard/fishery/graphfish3', component: graphfish3, meta: {requiresAuth: true, UACDashboard: true, UACfishery: true} },
    { path: '/dashboard/fishery/graphfish4', component: graphfish4, meta: {requiresAuth: true, UACDashboard: true, UACfishery: true} },
    { path: '/dashboard/fishery/graphfish5', component: graphfish5, meta: {requiresAuth: true, UACDashboard: true, UACfishery: true} },
    { path: '/dashboard/inet/graphinet1', component: graphinet1, meta: {requiresAuth: true, UACDashboard: true, UACfishery: true} },
    { path: '/sideMenu/dialogviewer/viewdataworkbench', component: viewdataworkbench, meta: {requiresAuth: true, UACMetadata: true} },
    { path: '*', redirect: '/' }
  ]
})

var UAC
// var UAC_DASHBOARD
// var VERSIONPLATFORM = 'platform'

router.beforeEach((to, from, next) => {
  if (to.matched.some((x) => x.meta.requiresAuth)) {
    if (localStorage.getItem('UAC')) {
      UAC = localStorage.getItem('UAC').split(',')
    } else {
      UAC = []
    }
    // if (localStorage.getItem('UAC_DASHBOARD')) {
    //   UAC_DASHBOARD = localStorage.getItem('UAC_DASHBOARD').split(',')
    // } else {
    //   UAC_DASHBOARD = []
    // }
    if (!localStorage.getItem('email')) {
      next({ path: '/' })
    } else {
      next()
    }
    return
  }
  next()
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((x) => x.meta.requiresAuthAdmin)) {
    if (localStorage.getItem('isAdmin') === 'false') {
      next({ path: '/sorry' })
    } else {
      next()
    }
    return
  }
  next()
})

// ==============
// LEFT MENU ZONE
// ==============
router.beforeEach((to, from, next) => {
  if (to.matched.some((x) => x.meta.UACMetadata)) {
    if (UAC.indexOf('*') !== -1) {
      next()
    } else if (UAC.indexOf('1') === -1) {
      next({ path: '/sorry' })
    } else {
      next()
    }
    return
  }
  next()
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((x) => x.meta.UACDashboard)) {
    if (UAC.indexOf('*') !== -1) {
      next()
    } else if (UAC.indexOf('2') === -1) {
      next({ path: '/sorry' })
    } else {
      next()
    }
    return
  }
  next()
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((x) => x.meta.UACUploadmornitoring)) {
    if (UAC.indexOf('*') !== -1) {
      next()
    } else if (UAC.indexOf('3') === -1) {
      next({ path: '/sorry' })
    } else {
      next()
    }
    return
  }
  next()
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((x) => x.meta.UACDataprep)) {
    if (UAC.indexOf('*') !== -1) {
      next()
    } else if (UAC.indexOf('4') === -1) {
      next({ path: '/sorry' })
    } else {
      next()
    }
    return
  }
  next()
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((x) => x.meta.UACIntelligistsharing)) {
    if (UAC.indexOf('*') !== -1) {
      next()
    } else if (UAC.indexOf('5') === -1) {
      next({ path: '/sorry' })
    } else {
      next()
    }
    return
  }
  next()
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((x) => x.meta.UACSocialMonitoring)) {
    if (UAC.indexOf('*') !== -1) {
      next()
    } else if (UAC.indexOf('7') === -1) {
      next({ path: '/sorry' })
    } else {
      next()
    }
    return
  }
  next()
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((x) => x.meta.UACGenerateWizard)) {
    if (UAC.indexOf('*') !== -1) {
      next()
    } else if (UAC.indexOf('8') === -1) {
      next({ path: '/sorry' })
    } else {
      next()
    }
    return
  }
  next()
})

// ==============
// DASHBOARD ZONE
// ==============
// UACbank: true} },
// UACeducation: true} },
// UACgovernment: true} },
// UACfinance: true} },
// UAChealth: true} },
// UACcommercial: true} },
// UACfishery: true} }

// router.beforeEach((to, from, next) => {
//   if (to.matched.some((x) => x.meta.UACbank)) {
//     if (UAC_DASHBOARD.indexOf('1') === -1) {
//       next()
//       // next({ path: '/sorry' })
//     } else {
//       next()
//     }
//     return
//   }
//   next()
// })

export default router
