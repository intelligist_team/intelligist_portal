FROM node:latest
WORKDIR /intelligist_portal/src
COPY ./package.json /intelligist_portal/src/package.json
RUN npm install
COPY . /intelligist_portal/src
RUN npm run build

FROM nginx:latest
WORKDIR /usr/share/nginx/html
COPY --from=0 /intelligist_portal/src/dist .
